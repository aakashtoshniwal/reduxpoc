import { IPatient } from './todo';
import { ADD_PATIENT, REMOVE_PATIENT, REMOVE_ALL_PATIENTS } from './actions';

export interface IAppState {
    patients: IPatient[];
    lastUpdate: Date;
}
export const INITIAL_STATE: IAppState = {
    patients: [],
    lastUpdate: null
}



export function rootReducer(state: IAppState, action): IAppState {
    switch (action.type) {
        case ADD_PATIENT:
            action.patient.id = state.patients.length + 1;    
            // debugger;
            return Object.assign({}, state, {
               
                patients: state.patients.concat(Object.assign({}, action.patient)),
                lastUpdate: new Date()

            })
        
        case REMOVE_PATIENT:
        // debugger;
            return Object.assign({}, state, {
                patients: state.patients.filter(t => t.id !== action.id),
                lastUpdate: new Date()
            })
        case REMOVE_ALL_PATIENTS:
            return Object.assign({}, state, {
                patients: [],
                lastUpdate: new Date()
            })
    }
    return state;
}