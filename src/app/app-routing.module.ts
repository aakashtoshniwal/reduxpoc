import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientOverviewComponent } from './patient-overview/patient-overview.component';

const routes: Routes = [

  // { path: '',outlet: 'PatientListComponent', component: PatientListComponent },
  // { path: '', outlet: 'PatientOverviewComponent',component: PatientOverviewComponent }
   
  {path: '', children: [
    { path: '',outlet: 'PatientListComponent', component: PatientListComponent },
   { path: '', outlet: 'PatientOverviewComponent',component: PatientOverviewComponent }
]},
  {path: 'patient', children: [
    { path: '',outlet: 'PatientListComponent', component: PatientListComponent },
   { path: '', outlet: 'PatientOverviewComponent',component: PatientOverviewComponent }
]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [PatientListComponent,PatientOverviewComponent]