import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../store';
import { ADD_PATIENT, REMOVE_PATIENT } from '../actions';
import { IPatient } from '../todo';
@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  @select() patients;
  model: IPatient = {
    id: 0,
    name: "",
    description: "",
    gender: "male"
    };
  constructor(private ngRedux: NgRedux<IAppState>) { }
  ngOnInit() {
  }
  onSubmit() {
    this.ngRedux.dispatch({type: ADD_PATIENT, patient: this.model});
  }
 
  removePatient(patient) {
    this.ngRedux.dispatch({type: REMOVE_PATIENT, id: patient.id });
  }
}