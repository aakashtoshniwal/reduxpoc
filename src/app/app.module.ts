import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { AppRoutingModule,routingComponents } from './app-routing.module';

import { IAppState, rootReducer, INITIAL_STATE } from './store';
import { PatientOverviewComponent } from './patient-overview/patient-overview.component';
import { PatientListComponent } from './patient-list/patient-list.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <== add the imports!

@NgModule({
  declarations: [
    AppComponent,
    PatientOverviewComponent,
    PatientListComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    FormsModule,                               // <========== Add this line!
    ReactiveFormsModule ,
    NgReduxModule,AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor (ngRedux: NgRedux<IAppState>) {
      ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}



